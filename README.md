## NixGui

A graphical frontend for the Nix package manager. It is currently under development and in the beginning stages. Currently, there is a cli program (nix_cli.py), which is used to test the editing of the home.nix file.

## Goal

The Goal is to have a gui for Nix, which can be used on every Linux distribution. The idea is to make the Nix package manager more accesible for new Linux users. It will be able to edit the home.nix file for the nix home manager.

## Planned features

- Installs Nix package manager
- Intalls nix home-manager
- Edit home.nix file in .config/home-manager with graphical frontend
- Search for packages in nix package manager and put them in home.nix
- Delete packages from home.nix
- Run home-manager switch command
- Update nix channels

