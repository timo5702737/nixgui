{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "timo";
  home.homeDirectory = "/home/timo";

  #allow nonfree packages:
  nixpkgs.config.allowUnfreePredicate = _: true;

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.05"; # Please read the comment before changing.

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = [
    # # Adds the 'hello' command to your environment. It prints a friendly
    # # "Hello, world!" when run.
    # pkgs.hello
	
    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # # fonts?
    # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')

	
	#video:
    	pkgs.clipgrab

	#pictures:
	pkgs.gimp
	pkgs.shotwell
	pkgs.inkscape
	pkgs.blender
	pkgs.drawio

	#audio:
	pkgs.ardour
	pkgs.reaper
	pkgs.lmms
	pkgs.hydrogen
	pkgs.zynaddsubfx-ntk
	pkgs.mixxx

    	pkgs.amberol
	pkgs.strawberry
    	pkgs.musikcube
	pkgs.spotify

	#development:
	pkgs.libsForQt5.kate
	pkgs.libsForQt5.kdevelop
	pkgs.gitkraken
	pkgs.neovim

    	#learning:
   	pkgs.kstars
    	pkgs.libsForQt5.kturtle


	#tools:	
	pkgs.tmux
	pkgs.neofetch
	pkgs.tldr
	pkgs.keepassxc
	pkgs.p7zip
	pkgs.fzf
	#pkgs.libsForQt5.kcalc
	pkgs.qalculate-qt
	pkgs.backintime
	pkgs.deja-dup
	pkgs.syncthing
	

	#system:
	pkgs.libsForQt5.konsole
	pkgs.mc

	#internet:
    	pkgs.brave
    	pkgs.firefox

    	#Kommunikation:
	#pkgs.element-desktop
	pkgs.thunderbird

	#documents:
	pkgs.libsForQt5.okular
	pkgs.onlyoffice-bin

	#games:
	pkgs.lutris
	#pkgs.sameboy
	pkgs.libsForQt5.ksudoku
	pkgs.mindustry
	pkgs.duckstation

	#fun:
	pkgs.lolcat
	pkgs.figlet

	#drivers:

	

  ];



  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    #config files:
	".dwm".source = ./config/.dwm;
	".config/musikcube".source = ./config/musikcube;	 
    	".config/kate".source = ./config/kate;
	#".config/REAPER".source = ~/Programme/Config/REAPER;
	#".thunderbird".source = ~/Programme/Config/.thunderbird;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';

    #desktop files:
    #".local/share/applications/org.kde.kate.desktop".source = ./desktop-files/org.kde.kate.desktop;
    #".local/share/applications/applications".source = ~/.nix-profile/share/applications;


  };

  # You can also manage environment variables but you will have to manually
  # source
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/timo/etc/profile.d/hm-session-vars.sh
  #
  # if you don't want to manage your shell through Home Manager.
  home.sessionVariables = {
    # EDITOR = "emacs";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
