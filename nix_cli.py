#!/bin/python

import glob
import os.path

pkglist = []
uninstallist = []
installist = []
nr = 0

def list_packages():
    nr = 0
    print("List of installed packages:")
    with open(r"home.nix", "r") as fp:
        for l_no, line in enumerate(fp):
            if "pkgs." in line:
                if "#" in line:
                    pass
                elif "allowUnfreePredicate" in line:
                    pass
                else:
                    nr = nr + 1
                    package=line.replace("pkgs.","")
                    package = package.strip()
                    print(nr, package)
                    pkglist.append([nr, package, l_no])

        #print(pkglist)


def deinstallation():
    print("Which packages shall be selected for deinstallation? Please type the number of a package and then press enter. To end selecting packages, press c")

    statusx = 1

    while statusx == 1:
        packagenr = input()
        if packagenr == "c":
           #print("Uninstalling packages:")
           #print(uninstallist)
           packagenr = 0
           statusx = 0
        packagenr = int(packagenr)
        if packagenr > len(pkglist):
            print("A package with Nr.", packagenr, "is not installed!")
        if 0 < packagenr < (len(pkglist) + 1):
            uninstallist.append(pkglist[packagenr-1])
    
def search():
    status = 1
    while status == 1:

        print("Which package are you looking for?")
        query = input()

        search = [ glob.glob("nixpkgs/pkgs/*/*/*" + query + "*"),
                glob.glob("nixpkgs/pkgs/*/*" + query + "*"),
                glob.glob("nixpkgs/pkgs/*/*/*/*" + query + "*") ]

        print("Avilable Packages containing searchterm " + query + ":")

        nr = 0
        pkglist = []

        for t in search:
            for x in t:
                pkgs = os.path.splitext(os.path.basename(x))
                nr = nr + 1
                print(nr, pkgs[0])
                pkglist.append([pkgs[0],x])

        print("which packages shall be installed? Please type the number, separated with comma.")

        install = input()

        for z in install:
            if z == ",":
                z = 0
            z = int(z)
            if z > nr:
                print("Package Nr.", z, "is not available!")
            if 0 < z < (nr + 1):
                installist.append(pkglist[z-1])

        print(installist)

        print("Search for more packages for installation? y/n:")
        answer = input()
        if answer == "n":
            status = 0
        if answer == "y":
            pass

def apply():
    for i in uninstallist:
        dline = i[2]
        print(dline)
        with open(r"home.nix", "r") as fp:
            lines = fp.readlines()
        with open(r"home.nix", "w") as fp:
            for number, line in enumerate(lines):
                if number not in [dline]:
                    fp.write(line)
    
    writeline = 39
    for i in installist:
        writeline = writeline + 1
        #with open(r"home.nix", "r") as fp:
        #    lines = fp.readlines()
        with open(r"home.nix", "w") as fp:
            fp.write 

            #for number, line in enumerate(lines):
            #    if number not in [writeline]:
            #        fp.write(line)
            #    if number in [writeline]:
            #        fp.write("hallo")
         
            
        print(i[0])

    print(uninstallist)
    print(installist)


list_packages()

program_status = 1

while program_status == 1:
        
    print("What do you want to do? To list installed packages, press l. To select packages for deinstallation, type d. To search for new packages, press s. To apply changes made, press a. To exit, press c.")

    answer = input()

    if answer == "l":
        list_packages()
    if answer == "d":
        deinstallation()
    if answer == "s":
        search()
    if answer == "a":
        apply()
    if answer == "c":
        exit()


